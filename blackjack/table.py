import os
import random

from cards import print_cards, compute_hand, get_all_cards

_POSITIONS = ['Stack', 'Dealer', 'Player']
_WON = 'You WON!'
_LOST = 'You LOST!'
_TIE = 'TIE!'
_OUTPUT = True

PLAYER = 'player'
DEALER = 'dealer'
STACK = 'stack'

TARGET = 21
STICK = 17


def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')


def shuffle_new_deck(ndecks=1):
    deck = get_all_cards() * ndecks
    deck = random.sample(deck, len(deck))
    assert len(deck) == 52 * ndecks
    return deck


def draw(game, player):
    c = game[STACK][0]
    game[STACK] = game[STACK][1:]
    game[player].append(c)


def bust(cards):
    return compute_hand(cards)[0] > TARGET


def display_outputs(state):
    global  _OUTPUT
    _OUTPUT = state


def print_tie():
    if _OUTPUT:
        print(_TIE)
    return 0


def print_won():
    if _OUTPUT:
        print(_WON)
    return 1


def print_lost():
    if _OUTPUT:
        print(_LOST)
    return -1


def print_tabel(game):
    if not _OUTPUT:
        return

    clear_screen()

    stack = print_cards("BG")
    player = print_cards(game[PLAYER])
    dealer = print_cards(game[DEALER])

    min, max = compute_hand(game[PLAYER])
    pstats = ' (' + str(min) + '-' + str(max) + ')'

    _, max = compute_hand(game[DEALER])
    dstats = ' (' + str(max) + ')'

    sstats = " (" + str(len(game[STACK])) + ")"

    print('\n')
    print(f'{_POSITIONS[0] + sstats:<20s}' +
          f'{_POSITIONS[1] + dstats:<40s}' +
          f'{_POSITIONS[2] + pstats:<40s}')

    for s, d, p in zip(stack, dealer, player):
        dcolors = d.count('\33')
        pcolors = p.count('\33')
        print(f'{s:<30s}{d:<{40+dcolors*5}s}{p:<{40+pcolors*5}s}')

    print()
