from cards import compute_hand


def the_brain(cards, dealer):
    """
    This method is called on every players turn in a BlackJack game.
    It returns True if the player wants to hit (draw another card) or False if the player wants to stay.
    You are free to use the 'compute_hand' function, which computes the minimum and maximum value of a list of cards
    (your hard hand (minimum) and your soft hand (maximum)).

    For example:

    >>> cards = ['H2', 'P3']
    >>> dealer = ['C9']
    >>> if the_brain(cards, dealer):
    ...     print('Player hits.')
    ... else:
    ...     print('Player stands')
    Player hits.

    :param cards: List of cards in the players hand.
    :param dealer: List of cards from the dealers hand.
    :return: True if the player wants to hit or False if the player wants to stay.
    """
    pass  # this is just a placeholder while there is no actual code, you can remove this line


def dumb(cards, dealer):
    pass  # this is just a placeholder while there is no actual code, you can remove this line


def human(cards, dealer):
    pass  # this is just a placeholder while there is no actual code, you can remove this line
