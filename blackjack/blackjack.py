from dealer import dealer
from player import dumb, the_brain, human
from table import *


def init(ndecks=1):
    stack = shuffle_new_deck(ndecks)

    game = {STACK: stack,
            DEALER: [],
            PLAYER: []}

    draw(game, PLAYER)
    draw(game, DEALER)
    draw(game, PLAYER)

    return game


def evaluate(game):
    dstat = compute_hand(game[DEALER])[1]
    pstat = compute_hand(game[PLAYER])
    if pstat[1] <= TARGET:
        pstat = pstat[1]
    else:
        pstat = pstat[0]

    if pstat > dstat:
        return print_won()
    elif pstat < dstat:
        return print_lost()
    else:
        return print_tie()


def play(player=dumb, ndecks=1):
    game = init(ndecks)
    print_tabel(game)

    while player(game[PLAYER], game[DEALER]):
        draw(game, PLAYER)
        print_tabel(game)

        if bust(game[PLAYER]):
            return print_lost()

    while dealer(game[DEALER]):
        draw(game, DEALER)
        print_tabel(game)

        if bust(game[DEALER]):
            return print_won()

    print_tabel(game)
    return evaluate(game)


def eval_player(player=dumb, nruns=50000, ndecks=1):
    display_outputs(False)

    wins, losses, ties = 0, 0, 0
    for i in range(nruns):
        res = play(player, ndecks)
        if res > 0:
            wins += 1
        elif res < 0:
            losses += 1
        else:
            ties += 1

        if i % 5000 == 0:
            print(f'\rWon:\t{wins} \t-> {wins * 100 / nruns:.2f}%\t'
                  f'Lost:\t{losses} \t-> {losses * 100 / nruns:.2f}%\t'
                  f'Tied:\t{ties} \t-> {ties * 100 / nruns:.2f}%', end='')

    print(f'\rWon:\t{wins} \t-> {wins * 100 / nruns:.2f}%\t'
          f'Lost:\t{losses} \t-> {losses * 100 / nruns:.2f}%\t'
          f'Tied:\t{ties} \t-> {ties * 100 / nruns:.2f}%')


if __name__ == '__main__':
    if input('Human player? [y/N]').lower().startswith('y'):
        display_outputs(True)
        play(human)
        while not input('Play another game? [Y/n]').lower().startswith('n'):
            play(human)
    else:
        print('Dumb player:')
        eval_player(dumb, 50000, 1)
        print('The brain:')
        eval_player(the_brain, 50000, 1)
