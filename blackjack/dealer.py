from cards import compute_hand
from table import TARGET, STICK


def dealer(cards):
    min, max = compute_hand(cards)

    val = max
    if val > TARGET:
        val = min

    return val < STICK
