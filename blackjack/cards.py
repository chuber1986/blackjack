_CARDS = {
    'H2':
        ''' _____ 
        |2    |
        |  v  |
        |     |
        |  v  |
        |____Z|''',
    'H3':
        ''' _____ 
        |3    |
        | v v |
        |     |
        |  v  |
        |____E|''',
    'H4':
        ''' _____ 
        |4    |
        | v v |
        |     |
        | v v |
        |____h|''',
    'H5':
        ''' _____ 
        |5    |
        | v v |
        |  v  |
        | v v |
        |____S|''',
    'H6':
        ''' _____
        |6    |
        | v v |
        | v v |
        | v v |
        |____9|''',
    'H7':
        ''' _____
        |7    |
        | v v |
        |v v v|
        | v v |
        |____L|''',
    'H8':
        ''' _____
        |8    |
        |v v v|
        | v v |
        |v v v|
        |____8|''',
    'H9':
        ''' _____
        |9    |
        |v v v|
        |v v v|
        |v v v|
        |____6|''',
    'H10':
        ''' _____ 
        |10 v |
        |v v v|
        |v v v|
        |v v v|
        |___0I|''',
    'HJ':
        ''' _____ 
        |J  ww|
        |   {)|
        |(v)% |
        | v % |
        |__%%[|''',
    'HQ':
        ''' _____ 
        |Q  ww|
        |   {(|
        |(v)%%|
        | v%%%|
        |_%%%O|''',
    'HK':
        ''' _____ 
        |K  WW|
        |   {)|
        |(v)%%|
        | v%%%|
        |_%%%>|''',
    'HA':
        ''' _____
        |A_ _ |
        |( v )|
        | \ / |
        |  .  |
        |____V|''',
    'T2':
        ''' _____ 
        |2    |
        |  o  |
        |     |
        |  o  |
        |____Z|''',
    'T3':
        ''' _____ 
        |3    |
        | o o |
        |     |
        |  o  |
        |____E|''',
    'T4':
        ''' _____ 
        |4    |
        | o o |
        |     |
        | o o |
        |____h|''',
    'T5':
        ''' _____ 
        |5    |
        | o o |
        |  o  |
        | o o |
        |____S|''',
    'T6':
        ''' _____
        |6    |
        | o o |
        | o o |
        | o o |
        |____9|''',
    'T7':
        ''' _____
        |7    |
        | o o |
        |o o o|
        | o o |
        |____L|''',
    'T8':
        ''' _____
        |8    |
        |o o o|
        | o o |
        |o o o|
        |____8|''',
    'T9':
        ''' _____
        |9    |
        |o o o|
        |o o o|
        |o o o|
        |____6|''',
    'T10':
        ''' _____ 
        |10 o |
        |o o o|
        |o o o|
        |o o o|
        |___0I|''',
    'TJ':
        ''' _____ 
        |J  ww|
        | /\{)|
        | \/% |
        |   % |
        |__%%[|''',
    'TQ':
        ''' _____ 
        |Q  ww|
        | /\{(|
        | \/%%|
        |  %%%|
        |_%%%O|''',
    'TK':
        ''' _____ 
        |K  WW|
        | /\{)|
        | \/%%|
        |  %%%|
        |_%%%>|''',
    'TA':
        ''' _____
        |A ^  |
        | / \ |
        | \ / |
        |  .  |
        |____V|''',
    'C2':
        ''' _____ 
        |2    |
        |  &  |
        |     |
        |  &  |
        |____Z|''',
    'C3':
        ''' _____ 
        |3    |
        | & & |
        |     |
        |  &  |
        |____E|''',
    'C4':
        ''' _____ 
        |4    |
        | & & |
        |     |
        | & & |
        |____h|''',
    'C5':
        ''' _____ 
        |5    |
        | & & |
        |  &  |
        | & & |
        |____S|''',
    'C6':
        ''' _____
        |6    |
        | & & |
        | & & |
        | & & |
        |____9|''',
    'C7':
        ''' _____
        |7    |
        | & & |
        |& & &|
        | & & |
        |____L|''',
    'C8':
        ''' _____
        |8    |
        |& & &|
        | & & |
        |& & &|
        |____8|''',
    'C9':
        ''' _____
        |9    |
        |& & &|
        |& & &|
        |& & &|
        |____6|''',
    'C10':
        ''' _____ 
        |10 & |
        |& & &|
        |& & &|
        |& & &|
        |___0I|''',
    'CJ':
        ''' _____ 
        |J  ww|
        | o {)|
        |o o% |
        | | % |
        |__%%[|''',
    'CQ':
        ''' _____ 
        |Q  ww|
        | o {(|
        |o o%%|
        | |%%%|
        |_%%%O|''',
    'CK':
        ''' _____ 
        |K  WW|
        | o {)|
        |o o%%|
        | |%%%|
        |_%%%>|''',
    'CA':
        ''' _____
        |A _  |
        | ( ) |
        |(_'_)|
        |  |  |
        |____V|''',
    'P2':
        ''' _____ 
        |2    |
        |  ^  |
        |     |
        |  ^  |
        |____Z|''',
    'P3':
        ''' _____ 
        |3    |
        | ^ ^ |
        |     |
        |  ^  |
        |____E|''',
    'P4':
        ''' _____ 
        |4    |
        | ^ ^ |
        |     |
        | ^ ^ |
        |____h|''',
    'P5':
        ''' _____ 
        |5    |
        | ^ ^ |
        |  ^  |
        | ^ ^ |
        |____S|''',
    'P6':
        ''' _____
        |6    |
        | ^ ^ |
        | ^ ^ |
        | ^ ^ |
        |____9|''',
    'P7':
        ''' _____
        |7    |
        | ^ ^ |
        |^ ^ ^|
        | ^ ^ |
        |____L|''',
    'P8':
        ''' _____
        |8    |
        |^ ^ ^|
        | ^ ^ |
        |^ ^ ^|
        |____8|''',
    'P9':
        ''' _____
        |9    |
        |^ ^ ^|
        |^ ^ ^|
        |^ ^ ^|
        |____6|''',
    'P10':
        ''' _____ 
        |10 ^ |
        |^ ^ ^|
        |^ ^ ^|
        |^ ^ ^|
        |___0I|''',
    'PJ':
        ''' _____ 
        |J  ww|
        | ^ {)|
        |(.)% |
        | | % |
        |__%%[|''',
    'PQ':
        ''' _____ 
        |Q  ww|
        | ^ {(|
        |(.)%%|
        | |%%%|
        |_%%%O|''',
    'PK':
        ''' _____ 
        |K  WW|
        | ^ {)|
        |(.)%%|
        | |%%%|
        |_%%%>|''',
    'PA':
        ''' _____
        |A .  |
        | /.\ |
        |(_._)|
        |  |  |
        |____V|''',
    'BG':
        ''' _____
        |\ ~ /|
        |}}:{{|
        |}}:{{|
        |}}:{{|
        |/_~_\|'''
}


_CARDS = {k: [l.strip() if i > 0 else l for i, l in enumerate(v.split('\n'))] for k, v in _CARDS.items()}


def _get_color(card='-'):
    c = card[0]
    if c == 'H':
        return '\033[91m'
    if c == 'C':
        return '\033[95m'
    if c == 'T':
        return '\033[93m'
    if c == 'P':
        return '\033[94m'
    if c == 'B':
        return '\033[96m'

    return '\033[00m'


def get_all_cards():
    return [c for c in _CARDS if c != 'BG']


def get_card_value(card):
    assert card is not None
    assert isinstance(card, str)
    assert card in _CARDS
    assert card != 'BG'

    val = card[1:]
    if val == 'A':
        return 1

    if val.isnumeric():
        return int(val)

    return 10


def compute_hand(cards):
    """
    This method comuptes you hard and soft hand. It sums up the values of all you cards counting aces as 1.
    The result is your soft hand value. If your hand contains an ace, 10 will be added to your soft hand value,
    what gives your hard hand value. If there are no aces in you hand, the soft and hard hand values are the same.

    For example:

    >>> cards = ['H2', 'PA']
    >>> dhard, dsoft = compute_hand(cards)

    :param cards: a list of string representing your cards
    :return: the values of your had and soft hand respectively
    """
    if not isinstance(cards, list):
        cards = [cards]

    sum = 0
    has_ace = 0
    for c in cards:
        value = get_card_value(c)
        has_ace = has_ace or value == 1
        sum += value

    hard = sum
    if sum < 12 and has_ace:
        soft = hard + 10
    else:
        soft = hard

    return hard, soft


def print_cards(cards, spaces=-4):
    if not isinstance(cards, list):
        cards = [cards]

    for card in cards:
        assert card in _CARDS

    assert -6 < spaces < 6
    sp = ' ' * spaces

    lines = []
    for l in range(6):
        line = ''
        for c in cards[:-1]:
            line += _get_color(c) + _CARDS[c][l][:len(_CARDS[c][l])+spaces] + sp
        line += _get_color(cards[-1]) + _CARDS[cards[-1]][l] + _get_color()
        lines.append(line)

    return lines
